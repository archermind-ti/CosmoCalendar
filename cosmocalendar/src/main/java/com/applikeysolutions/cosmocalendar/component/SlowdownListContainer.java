package com.applikeysolutions.cosmocalendar.component;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

public class SlowdownListContainer extends ListContainer {
    public SlowdownListContainer(Context context) {
        super(context);
    }

    public SlowdownListContainer(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public SlowdownListContainer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void scrollBy(int x, int y) {
        super.scrollBy(x, y);
    }
}
