package com.applikeysolutions.cosmocalendar.component.delegate;

import com.applikeysolutions.cosmocalendar.utils.PixelMapUtils;
import com.applikeysolutions.cosmocalendar.ResourceTable;
import com.applikeysolutions.cosmocalendar.component.CalendarComponent;
import com.applikeysolutions.cosmocalendar.component.CircleAnimationText;
import com.applikeysolutions.cosmocalendar.model.Day;
import com.applikeysolutions.cosmocalendar.provider.DaysProvider;
import com.applikeysolutions.cosmocalendar.provider.MonthProvider;
import com.applikeysolutions.cosmocalendar.selection.BaseSelectionManager;
import com.applikeysolutions.cosmocalendar.selection.MultipleSelectionManager;
import com.applikeysolutions.cosmocalendar.selection.RangeSelectionManager;
import com.applikeysolutions.cosmocalendar.selection.SelectionState;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.media.image.PixelMap;

public class DayDelegate extends BaseDelegate {

    private MonthProvider monthProvider;

    public DayDelegate(CalendarComponent calendarView, MonthProvider monthProvider) {
        this.calendarView = calendarView;
        this.monthProvider = monthProvider;
    }

    public Component onParse(LayoutScatter layoutScatter, ComponentContainer root, int itemType) {
        Component component = layoutScatter.parse(ResourceTable.Layout_item_day, root, false);
        return component;
    }

    public void onBindData(DaysProvider provider, Day day, Component itemComponent, int position) {
        final BaseSelectionManager selectionManager = monthProvider.getSelectionManager();

        CircleAnimationText text = (CircleAnimationText) itemComponent.findComponentById(ResourceTable.Id_tv_day_number);
        text.setText(String.valueOf(day.getDayNumber()));

        boolean isSelected = selectionManager.isDaySelected(day);
        if (isSelected && !day.isDisabled()) {
            select(day, text, selectionManager);
        } else {
            unselect(day, text);
        }

        if (day.isCurrent()) {
            addCurrentDayIcon(isSelected, text);
        }

        if (day.isDisabled()) {
            text.setTextColor(new Color(calendarView.getDisabledDayTextColor()));
        }

        itemComponent.setClickedListener(component -> {
            if (!day.isDisabled()) {
                selectionManager.toggleDay(day);
                if (selectionManager instanceof MultipleSelectionManager) {
                    provider.notifyDataSetItemChanged(position);
                } else {
                    monthProvider.notifyDataChanged();
                }
            }
        });
    }

    private void addCurrentDayIcon(boolean isSelected, Text tvDay) {
        tvDay.addDrawTask((component, canvas) -> {
            PixelMap pixelMap = isSelected
                    ? PixelMapUtils.getPixelMapFromResource(tvDay.getContext(), calendarView.getCurrentDaySelectedIconRes())
                    : PixelMapUtils.getPixelMapFromResource(tvDay.getContext(), calendarView.getCurrentDayIconRes());
            PixelMapHolder pixelMapHolder = new PixelMapHolder(pixelMap);
            canvas.drawPixelMapHolder(pixelMapHolder, component.getWidth() / 2 - (pixelMap.getImageInfo().size.width / 2), 10, new Paint());
        });
    }

    private void select(Day day, Text dayText, BaseSelectionManager selectionManager) {
        if (day.isFromConnectedCalendar()) {
            if (day.isDisabled()) {
                dayText.setTextColor(new Color(day.getConnectedDaysDisabledTextColor()));
            } else {
                dayText.setTextColor(new Color(day.getConnectedDaysSelectedTextColor()));
            }
        } else {
            dayText.setTextColor(new Color(calendarView.getSelectedDayTextColor()));
        }

        SelectionState state;
        if (selectionManager instanceof RangeSelectionManager) {
            state = ((RangeSelectionManager) selectionManager).getSelectedState(day);
        } else {
            state = SelectionState.SINGLE_DAY;
        }
        animateDay(state, day, dayText);
    }

    private void unselect(Day day, Text dayText) {
        int textColor;
        if (day.isFromConnectedCalendar()) {
            if (day.isDisabled()) {
                textColor = day.getConnectedDaysDisabledTextColor();
            } else {
                textColor = day.getConnectedDaysTextColor();
            }
        } else if (day.isWeekend()) {
            textColor = calendarView.getWeekendDayTextColor();
        } else {
            textColor = calendarView.getDayTextColor();
        }
        day.setSelectionCircleDrawed(false);
        dayText.setTextColor(new Color(textColor));
        ((CircleAnimationText) dayText).clearView();
    }

    private void animateDay(SelectionState state, Day day, Text dayText) {
        if (day.getSelectionState() != state) {
            if (day.isSelectionCircleDrawed() && state == SelectionState.SINGLE_DAY) {
                ((CircleAnimationText) dayText).showAsSingleCircle(calendarView);
            } else if (day.isSelectionCircleDrawed() && state == SelectionState.START_RANGE_DAY) {
                ((CircleAnimationText) dayText).showAsStartCircle(calendarView, false);
            } else if (day.isSelectionCircleDrawed() && state == SelectionState.END_RANGE_DAY) {
                ((CircleAnimationText) dayText).showAsEndCircle(calendarView, false);
            } else {
                ((CircleAnimationText) dayText).setSelectionStateAndAnimate(state, calendarView, day);
            }
        } else {
            switch (state) {
                case SINGLE_DAY:
                    if (day.isSelectionCircleDrawed()) {
                        ((CircleAnimationText) dayText).showAsSingleCircle(calendarView);
                    } else {
                        ((CircleAnimationText) dayText).setSelectionStateAndAnimate(state, calendarView, day);
                    }
                    break;

                case RANGE_DAY:
                    ((CircleAnimationText) dayText).setSelectionStateAndAnimate(state, calendarView, day);
                    break;

                case START_RANGE_DAY_WITHOUT_END:
                    if (day.isSelectionCircleDrawed()) {
                        ((CircleAnimationText) dayText).showAsStartCircleWithouEnd(calendarView, false);
                    } else {
                        ((CircleAnimationText) dayText).setSelectionStateAndAnimate(state, calendarView, day);
                    }
                    break;

                case START_RANGE_DAY:
                    if (day.isSelectionCircleDrawed()) {
                        ((CircleAnimationText) dayText).showAsStartCircle(calendarView, false);
                    } else {
                        ((CircleAnimationText) dayText).setSelectionStateAndAnimate(state, calendarView, day);
                    }
                    break;

                case END_RANGE_DAY:
                    if (day.isSelectionCircleDrawed()) {
                        ((CircleAnimationText) dayText).showAsEndCircle(calendarView, false);
                    } else {
                        ((CircleAnimationText) dayText).setSelectionStateAndAnimate(state, calendarView, day);
                    }
                    break;
            }
        }
    }
}
