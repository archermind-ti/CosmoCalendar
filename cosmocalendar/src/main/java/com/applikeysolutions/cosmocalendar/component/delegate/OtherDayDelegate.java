package com.applikeysolutions.cosmocalendar.component.delegate;

import com.applikeysolutions.cosmocalendar.ResourceTable;
import com.applikeysolutions.cosmocalendar.component.CalendarComponent;
import com.applikeysolutions.cosmocalendar.model.Day;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

public class OtherDayDelegate {

    private CalendarComponent calendarView;

    public OtherDayDelegate(CalendarComponent calendarView) {
        this.calendarView = calendarView;
    }

    public Component onParse(LayoutScatter layoutScatter, ComponentContainer parent, int viewType) {
        return layoutScatter.parse(ResourceTable.Layout_item_other_day, parent, false);
    }

    public void onBindData(Day day, Component component, int position) {
        Text tvDay = (Text) component.findComponentById(ResourceTable.Id_tv_day_number);
        tvDay.setText(String.valueOf(day.getDayNumber()));
        tvDay.setTextColor(new Color(calendarView.getOtherDayTextColor()));
    }
}
