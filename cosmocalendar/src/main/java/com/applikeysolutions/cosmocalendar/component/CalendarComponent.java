package com.applikeysolutions.cosmocalendar.component;

import com.applikeysolutions.cosmocalendar.FetchMonthsAsyncTask;
import com.applikeysolutions.cosmocalendar.ResourceTable;
import com.applikeysolutions.cosmocalendar.component.delegate.MonthDelegate;
import com.applikeysolutions.cosmocalendar.listeners.OnMonthChangeListener;
import com.applikeysolutions.cosmocalendar.model.Day;
import com.applikeysolutions.cosmocalendar.model.Month;
import com.applikeysolutions.cosmocalendar.provider.MonthProvider;
import com.applikeysolutions.cosmocalendar.selection.*;
import com.applikeysolutions.cosmocalendar.selection.selectionbar.MultipleSelectionBarProvider;
import com.applikeysolutions.cosmocalendar.selection.selectionbar.SelectionBarItem;
import com.applikeysolutions.cosmocalendar.settings.SettingsManager;
import com.applikeysolutions.cosmocalendar.settings.appearance.AppearanceInterface;
import com.applikeysolutions.cosmocalendar.settings.date.DateInterface;
import com.applikeysolutions.cosmocalendar.settings.lists.CalendarListsInterface;
import com.applikeysolutions.cosmocalendar.settings.lists.DisabledDaysCriteria;
import com.applikeysolutions.cosmocalendar.settings.lists.connected_days.ConnectedDays;
import com.applikeysolutions.cosmocalendar.settings.lists.connected_days.ConnectedDaysManager;
import com.applikeysolutions.cosmocalendar.settings.selection.SelectionInterface;
import com.applikeysolutions.cosmocalendar.utils.AsyncTask;
import com.applikeysolutions.cosmocalendar.utils.CalendarUtils;
import com.applikeysolutions.cosmocalendar.utils.Constants;
import com.applikeysolutions.cosmocalendar.utils.SelectionType;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.Pair;

import java.util.*;

public class CalendarComponent extends DependentLayout
        implements OnDaySelectedListener, SelectionInterface, AppearanceInterface, DateInterface
        , CalendarListsInterface, MultipleSelectionBarProvider.ListItemClickListener, OnMonthChangeListener {
    public static final HiLogLabel TAG = new HiLogLabel(3, 0xD001100, "CalendarComponent");
    private SlowdownListContainer rvMonths;
    private TableLayoutManager tableLayoutManager;
    private StackLayout flBottomSelectionBar;
    private ListContainer lcMultipleSelectedList;
    private DirectionalLayout dlRangeSelection;
    private DirectionalLayout dlDaysOfWeekTitles;
    private MonthProvider monthProvider;
    private MultipleSelectionBarProvider multipleSelectionBarProvider;
    private SettingsManager settingsManager;
    private BaseSelectionManager selectionManager;
    private DependentLayout flNavigationButtons;
    private Image ivPrevious, ivNext;
    private FetchMonthsAsyncTask asyncTask;
    private int lastVisibleMonthPosition = SettingsManager.DEFAULT_MONTH_COUNT / 2;
    private List<Day> selectedDays;
    private OnMonthChangeListener onMonthChangeListener;


    private static final String ATTR_ORIENTATION = "orientation";
    private static final String ATTR_FIRST_DAY_OF_THE_WEEK = "firstDayOfTheWeek";
    private static final String ATTR_SELECTION_TYPE = "selectionType";
    private static final String ATTR_CALENDAR_BACKGROUND_COLOR = "calendarBackgroundColor";
    private static final String ATTR_MONTH_TEXT_COLOR = "monthTextColor";
    private static final String ATTR_OTHER_DAY_TEXT_COLOR = "otherDayTextColor";
    private static final String ATTR_DAY_TEXT_COLOR = "dayTextColor";
    private static final String ATTR_WEEKEND_DAY_TEXT_COLOR = "weekendDayTextColor";
    private static final String ATTR_WEEK_DAY_TITLE_TEXT_COLOR = "weekDayTitleTextColor";
    private static final String ATTR_SELECTED_DAY_TEXT_COLOR = "selectedDayTextColor";
    private static final String ATTR_SELECTED_DAY_BACKGROUND_COLOR = "selectedDayBackgroundColor";
    private static final String ATTR_SELECTED_DAY_BACKGROUND_START_COLOR = "selectedDayBackgroundStartColor";
    private static final String ATTR_SELECTED_DAY_BACKGROUND_END_COLOR = "selectedDayBackgroundEndColor";
    private static final String ATTR_CURRENT_DAY_TEXT_COLOR = "currentDayTextColor";
    private static final String ATTR_DISABLED_DAY_TEXT_COLOR = "disabledDayTextColor";
    private static final String ATTR_CURRENT_DAY_ICON_RES = "currentDayIconRes";
    private static final String ATTR_CURRENT_DAY_SELECTED_ICON_RES = "currentDaySelectedIconRes";
    private static final String ATTR_CONNECTED_DAY_ICON_RES = "connectedDayIconRes";
    private static final String ATTR_CONNECTED_DAY_SELECTED_ICON_RES = "connectedDaySelectedIconRes";
    private static final String ATTR_CONNECTED_DAY_ICON_POSITION = "connectedDayIconPosition";
    private static final String ATTR_SELECTION_BAR_MONTH_TEXT_COLOR = "selectionBarMonthTextColor";
    private static final String ATTR_PREVIOUS_MONTH_ICON_RES = "previousMonthIconRes";
    private static final String ATTR_NEXT_MONTH_ICON_RES = "nextMonthIconRes";

    public CalendarComponent(Context context) {
        this(context, null);
    }

    public CalendarComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public CalendarComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_layout_calendar, this, true);
        handleAttributes(attrSet);
        setSelectionManager();
        initView();
    }

    private void initView() {
        setDaysOfWeekTitles();
        initList();
        initBottomSelectionBar();

        if (settingsManager.getCalendarOrientation() == HORIZONTAL) {
            initNavigationButtons();
        }
    }

    private void initNavigationButtons() {
        flNavigationButtons = (DependentLayout) findComponentById(ResourceTable.Id_dl_calendar_navigation);
        setPreviousNavigationButton();
        setNextNavigationButton();
    }

    public void setOnMonthChangeListener(OnMonthChangeListener onMonthChangeListener) {
        this.onMonthChangeListener = onMonthChangeListener;
    }

    private void createDaysOfWeekTitle() {
        if (dlDaysOfWeekTitles == null) {
            dlDaysOfWeekTitles = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_days_of_week_titles);
        } else {
            dlDaysOfWeekTitles.removeAllComponents();
        }

        int screenWidth = AttrHelper.fp2px(mContext.getResourceManager().getDeviceCapability().width, mContext);
        int width = screenWidth / Constants.DAYS_IN_WEEK;
        DirectionalLayout.LayoutConfig textViewParam = new DirectionalLayout.LayoutConfig(width, width);
        textViewParam.weight = 1;
        for (String title : CalendarUtils.createWeekDayTitles(settingsManager.getFirstDayOfWeek())) {
            Text tvDayTitle = new Text(getContext());
            tvDayTitle.setText(title);
            tvDayTitle.setTextSize(16, Text.TextSizeType.VP);
            tvDayTitle.setLayoutConfig(textViewParam);
            tvDayTitle.setTextAlignment(TextAlignment.CENTER);
            dlDaysOfWeekTitles.addComponent(tvDayTitle);
        }

        //TODO 设置背景颜色
        //dlDaysOfWeekTitles.setBackground(R.drawable.border_top_bottom);
    }

    private void showDaysOfWeekTitle() {
        dlDaysOfWeekTitles.setVisibility(Component.VISIBLE);
    }

    private void hideDaysOfWeekTitle() {
        dlDaysOfWeekTitles.setVisibility(Component.HIDE);
    }

    private void setPreviousNavigationButton() {
        ivPrevious = (Image) findComponentById(ResourceTable.Id_iv_previous_month);
        ivPrevious.setPixelMap(settingsManager.getPreviousMonthIconRes());
        ivPrevious.setClickedListener(component -> goToPreviousMonth());
    }

    public void goToPreviousMonth() {
        if (lastVisibleMonthPosition != 0) {
            rvMonths.scrollTo(lastVisibleMonthPosition - 1);
        }
    }

    public void goToNextMonth() {
        if (lastVisibleMonthPosition != monthProvider.getData().size() - 1) {
            rvMonths.scrollTo(lastVisibleMonthPosition + 1);
        }
    }

    private void setNextNavigationButton() {
        ivNext = (Image) findComponentById(ResourceTable.Id_iv_next_month);
        ivNext.setPixelMap(settingsManager.getNextMonthIconRes());
        ivNext.setClickedListener(component -> goToNextMonth());
    }

    private void handleAttributes(AttrSet attrs) {
        settingsManager = new SettingsManager();

        boolean hasAttr;
        int orientation = SettingsManager.DEFAULT_ORIENTATION;
        hasAttr = attrs.getAttr(ATTR_ORIENTATION).isPresent();
        if (hasAttr) {
            orientation = attrs.getAttr(ATTR_ORIENTATION).get().getIntegerValue();
        }
        int firstDayOfWeek = SettingsManager.DEFAULT_FIRST_DAY_OF_WEEK;
        hasAttr = attrs.getAttr(ATTR_FIRST_DAY_OF_THE_WEEK).isPresent();
        if (hasAttr) {
            firstDayOfWeek = attrs.getAttr(ATTR_FIRST_DAY_OF_THE_WEEK).get().getIntegerValue();
        }
        int selectionType = SettingsManager.DEFAULT_SELECTION_TYPE.getValue();
        hasAttr = attrs.getAttr(ATTR_SELECTION_TYPE).isPresent();
        if (hasAttr) {
            selectionType = attrs.getAttr(ATTR_SELECTION_TYPE).get().getIntegerValue();
        }
        boolean showDaysOfWeekTitle = orientation != Component.HORIZONTAL;
        boolean showDaysOfWeek = orientation == Component.HORIZONTAL;

        int calendarBackgroundColor = 0xffffffff;
        hasAttr = attrs.getAttr(ATTR_SELECTION_TYPE).isPresent();
        if (hasAttr) {
            calendarBackgroundColor = attrs.getAttr(ATTR_CALENDAR_BACKGROUND_COLOR).get().getColorValue().getValue();
        }
        int monthTextColor = 0xff595f66;
        hasAttr = attrs.getAttr(ATTR_MONTH_TEXT_COLOR).isPresent();
        if (hasAttr) {
            monthTextColor = attrs.getAttr(ATTR_MONTH_TEXT_COLOR).get().getColorValue().getValue();
        }
        int otherDayTextColor = 0xffc9cacd;
        hasAttr = attrs.getAttr(ATTR_OTHER_DAY_TEXT_COLOR).isPresent();
        if (hasAttr) {
            otherDayTextColor = attrs.getAttr(ATTR_OTHER_DAY_TEXT_COLOR).get().getColorValue().getValue();
        }
        int dayTextColor = 0xff4a4a4a;
        hasAttr = attrs.getAttr(ATTR_DAY_TEXT_COLOR).isPresent();
        if (hasAttr) {
            dayTextColor = attrs.getAttr(ATTR_DAY_TEXT_COLOR).get().getColorValue().getValue();
        }
        int weekendDayTextColor = 0xff00cf78;
        hasAttr = attrs.getAttr(ATTR_WEEKEND_DAY_TEXT_COLOR).isPresent();
        if (hasAttr) {
            weekendDayTextColor = attrs.getAttr(ATTR_WEEKEND_DAY_TEXT_COLOR).get().getColorValue().getValue();
        }
        int weekDayTitleTextColor = 0xff91959b;
        hasAttr = attrs.getAttr(ATTR_WEEK_DAY_TITLE_TEXT_COLOR).isPresent();
        if (hasAttr) {
            weekDayTitleTextColor = attrs.getAttr(ATTR_WEEK_DAY_TITLE_TEXT_COLOR).get().getColorValue().getValue();
        }
        int selectedDayTextColor = 0xffffffff;
        hasAttr = attrs.getAttr(ATTR_SELECTED_DAY_TEXT_COLOR).isPresent();
        if (hasAttr) {
            selectedDayTextColor = attrs.getAttr(ATTR_SELECTED_DAY_TEXT_COLOR).get().getColorValue().getValue();
        }
        int selectedDayBackgroundColor = 0xff33d993;
        hasAttr = attrs.getAttr(ATTR_SELECTED_DAY_BACKGROUND_COLOR).isPresent();
        if (hasAttr) {
            selectedDayBackgroundColor = attrs.getAttr(ATTR_SELECTED_DAY_BACKGROUND_COLOR).get().getColorValue().getValue();
        }
        int selectedDayBackgroundStartColor = 0xff00cf7d;
        hasAttr = attrs.getAttr(ATTR_SELECTED_DAY_BACKGROUND_START_COLOR).isPresent();
        if (hasAttr) {
            selectedDayBackgroundStartColor = attrs.getAttr(ATTR_SELECTED_DAY_BACKGROUND_START_COLOR).get().getColorValue().getValue();
        }
        int selectedDayBackgroundEndColor = 0xff00a564;
        hasAttr = attrs.getAttr(ATTR_SELECTED_DAY_BACKGROUND_END_COLOR).isPresent();
        if (hasAttr) {
            selectedDayBackgroundEndColor = attrs.getAttr(ATTR_SELECTED_DAY_BACKGROUND_END_COLOR).get().getColorValue().getValue();
        }
        int currentDayTextColor = 0xff4a4a4a;
        hasAttr = attrs.getAttr(ATTR_CURRENT_DAY_TEXT_COLOR).isPresent();
        if (hasAttr) {
            currentDayTextColor = attrs.getAttr(ATTR_CURRENT_DAY_TEXT_COLOR).get().getColorValue().getValue();
        }
        int currentDayIconRes = ResourceTable.Media_ic_triangle_unslected;
        int currentDaySelectedIconRes = ResourceTable.Media_ic_triangle_selected;
        int connectedDayIconRes = 0;
        int connectedDaySelectedIconRes = 0;
        int connectedDayIconPosition = SettingsManager.DEFAULT_CONNECTED_DAY_ICON_POSITION;
        int disabledDayTextColor = 0xffc9cacd;
        hasAttr = attrs.getAttr(ATTR_DISABLED_DAY_TEXT_COLOR).isPresent();
        if (hasAttr) {
            disabledDayTextColor = attrs.getAttr(ATTR_DISABLED_DAY_TEXT_COLOR).get().getColorValue().getValue();
        }
        int selectionBarMonthTextColor = 0xffc9cacd;
        hasAttr = attrs.getAttr(ATTR_SELECTION_BAR_MONTH_TEXT_COLOR).isPresent();
        if (hasAttr) {
            selectionBarMonthTextColor = attrs.getAttr(ATTR_SELECTION_BAR_MONTH_TEXT_COLOR).get().getColorValue().getValue();
        }
        int previousMonthIconRes = ResourceTable.Media_ic_chevron_left_gray;
        int nextMonthIconRes = ResourceTable.Media_ic_chevron_right_gray;

        setBackgroundColor(calendarBackgroundColor);
        settingsManager.setCalendarBackgroundColor(calendarBackgroundColor);
        settingsManager.setMonthTextColor(monthTextColor);
        settingsManager.setOtherDayTextColor(otherDayTextColor);
        settingsManager.setDayTextColor(dayTextColor);
        settingsManager.setWeekendDayTextColor(weekendDayTextColor);
        settingsManager.setWeekDayTitleTextColor(weekDayTitleTextColor);
        settingsManager.setSelectedDayTextColor(selectedDayTextColor);
        settingsManager.setSelectedDayBackgroundColor(selectedDayBackgroundColor);
        settingsManager.setSelectedDayBackgroundStartColor(selectedDayBackgroundStartColor);
        settingsManager.setSelectedDayBackgroundEndColor(selectedDayBackgroundEndColor);
        settingsManager.setConnectedDayIconRes(connectedDayIconRes);
        settingsManager.setConnectedDaySelectedIconRes(connectedDaySelectedIconRes);
        settingsManager.setConnectedDayIconPosition(connectedDayIconPosition);
        settingsManager.setDisabledDayTextColor(disabledDayTextColor);
        settingsManager.setSelectionBarMonthTextColor(selectionBarMonthTextColor);
        settingsManager.setCurrentDayTextColor(currentDayTextColor);
        settingsManager.setCurrentDayIconRes(currentDayIconRes);
        settingsManager.setCurrentDaySelectedIconRes(currentDaySelectedIconRes);
        settingsManager.setCalendarOrientation(orientation);
        settingsManager.setFirstDayOfWeek(firstDayOfWeek);
        settingsManager.setShowDaysOfWeek(showDaysOfWeek);
        settingsManager.setShowDaysOfWeekTitle(showDaysOfWeekTitle);
        settingsManager.setSelectionType(SelectionType.getType(selectionType));
        settingsManager.setPreviousMonthIconRes(previousMonthIconRes);
        settingsManager.setNextMonthIconRes(nextMonthIconRes);
    }

    public void initList() {
        rvMonths = (SlowdownListContainer) findComponentById(ResourceTable.Id_lc_months);
        rvMonths.setLongClickable(false);
        setListOrientation();
        rvMonths.addScrolledListener(pagingScrollListener);
        rvMonths.setScrollListener(() -> {
            int currentPosition = rvMonths.getFirstVisibleItemPosition();
            if (currentPosition != lastVisibleMonthPosition) {
                lastVisibleMonthPosition = rvMonths.getFirstVisibleItemPosition();
                if (onMonthChangeListener != null) {
                    onMonthChangeListener.onMonthChanged(monthProvider.getData().get(lastVisibleMonthPosition));
                }
            }
        });

    }

    private void setListOrientation() {
        tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(1);
        if (settingsManager.getCalendarOrientation() == Component.HORIZONTAL) {
            tableLayoutManager.setOrientation(Component.VERTICAL);
            rvMonths.setOrientation(Component.HORIZONTAL);
        } else {
            tableLayoutManager.setOrientation(Component.HORIZONTAL);
            rvMonths.setOrientation(Component.VERTICAL);
        }
        rvMonths.setLayoutManager(tableLayoutManager);
        monthProvider = createAdapter();
        rvMonths.setItemProvider(monthProvider);
        rvMonths.scrollTo(lastVisibleMonthPosition);
    }

    private MonthProvider createAdapter() {
        return new MonthProvider.MonthAdapterBuilder()
                .setOrientation(settingsManager.getCalendarOrientation())
                .setMonths(CalendarUtils.createInitialMonths(settingsManager))
                .setMonthDelegate(new MonthDelegate(settingsManager))
                .setCalendarView(this)
                .setSelectionManager(selectionManager)
                .createMonthProvider();
    }

    private Component.ScrolledListener pagingScrollListener = new ScrolledListener() {
        @Override
        public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
            int visibleIndexCount = rvMonths.getFirstVisibleItemPosition();
            if (visibleIndexCount < 2) {
                loadAsyncMonths(false);
            }
            if (visibleIndexCount > monthProvider.getData().size() - 4) {
                loadAsyncMonths(true);
            }
        }

        @Override
        public void scrolledStageUpdate(Component component, int newStage) {
            if (getCalendarOrientation() == Component.HORIZONTAL) {
                multipleSelectionBarProvider.notifyDataChanged();

                boolean show = newStage != Component.SCROLL_NORMAL_STAGE;
                ivPrevious.setVisibility(show ? Component.VISIBLE : Component.HIDE);
                ivNext.setVisibility(show ? Component.VISIBLE : Component.HIDE);
            }
        }
    };

    private void loadAsyncMonths(final boolean future) {
        if (asyncTask != null && (asyncTask.getStatus() == AsyncTask.Status.PENDING || asyncTask.getStatus() == AsyncTask.Status.RUNNING))
            return;

        asyncTask = new FetchMonthsAsyncTask();
        Month month;

        if (future) {
            month = monthProvider.getData().get(monthProvider.getData().size() - 1);
        } else {
            month = monthProvider.getData().get(0);
        }
        asyncTask.execute(new FetchMonthsAsyncTask.FetchParams(future, month, settingsManager, monthProvider, SettingsManager.DEFAULT_MONTH_COUNT));
    }

    private void initBottomSelectionBar() {
        flBottomSelectionBar = (StackLayout) findComponentById(ResourceTable.Id_sl_bottom_selection_bar);
        flBottomSelectionBar.setVisibility(settingsManager.getCalendarOrientation() == Component.HORIZONTAL ? VISIBLE : HIDE);
        initMultipleSelectionBarList();
        initRangeSelectionLayout();
    }

    public void initMultipleSelectionBarList() {
        lcMultipleSelectedList = (ListContainer) findComponentById(ResourceTable.Id_lc_multiple_selected);
        multipleSelectionBarProvider = new MultipleSelectionBarProvider(getContext(), this, this);
        lcMultipleSelectedList.setItemProvider(multipleSelectionBarProvider);
    }

    public void initRangeSelectionLayout() {
        dlRangeSelection = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_selection_bar_range);
        dlRangeSelection.setVisibility(Component.HIDE);
    }

    private void setSelectionManager() {
        switch (getSelectionType()) {
            case SINGLE:
                selectionManager = new SingleSelectionManager(this);
                break;

            case MULTIPLE:
                selectionManager = new MultipleSelectionManager(this);
                break;

            case RANGE:
                selectionManager = new RangeSelectionManager(this);
                break;

            case NONE:
                selectionManager = new NoneSelectionManager();
                break;
        }
    }

    public void setSelectionManager(BaseSelectionManager selectionManager) {
        this.selectionManager = selectionManager;
        monthProvider.setSelectionManager(selectionManager);
        update();
    }

    public BaseSelectionManager getSelectionManager() {
        return selectionManager;
    }

    public void scrollToPosition(int position) {
        rvMonths.scrollTo(position);
    }

    @Override
    public SelectionType getSelectionType() {
        return settingsManager.getSelectionType();
    }

    @Override
    public void setSelectionType(SelectionType selectionType) {
        settingsManager.setSelectionType(selectionType);
        setSelectionManager();
        monthProvider.setSelectionManager(selectionManager);
        setSelectionBarVisibility();

        multipleSelectionBarProvider.setData(new ArrayList<SelectionBarItem>());
        selectionManager.clearSelections();
        if (selectionManager instanceof MultipleSelectionManager) {
            ((MultipleSelectionManager) selectionManager).clearCriteriaList();
        }

        update();
    }

    @Override
    public void onDaySelected() {
        selectedDays = getSelectedDays();
        displaySelectedDays();
    }

    public List<Day> getSelectedDays() {
        List<Day> selectedDays = new ArrayList<>();
        for (Iterator<Month> monthIterator = monthProvider.getData().iterator(); monthIterator.hasNext(); ) {
            Month month = monthIterator.next();
            for (Iterator<Day> dayIterator = month.getDaysWithoutTitlesAndOnlyCurrent().iterator(); dayIterator.hasNext(); ) {
                Day day = dayIterator.next();
                if (selectionManager.isDaySelected(day)) {
                    selectedDays.add(day);
                }
            }
        }
        return selectedDays;
    }

    private void displaySelectedDays() {
        switch (settingsManager.getSelectionType()) {
            case MULTIPLE:
                displaySelectedDaysMultiple();
                break;

            case RANGE:
                displaySelectedDaysRange();
                break;

            default:
                dlRangeSelection.setVisibility(HIDE);
                break;
        }
    }

    private void displaySelectedDaysMultiple() {
        multipleSelectionBarProvider.setData(CalendarUtils.getSelectedDayListForMultipleMode(selectedDays));
    }

    private void displaySelectedDaysRange() {
        if (selectionManager instanceof RangeSelectionManager) {
            Pair<Day, Day> days = ((RangeSelectionManager) selectionManager).getDays();
            if (days != null) {
                lcMultipleSelectedList.setVisibility(HIDE);
                dlRangeSelection.setVisibility(VISIBLE);
                Text tvStartRangeTitle = (Text) findComponentById(ResourceTable.Id_tv_range_start_date);
                tvStartRangeTitle.setText(CalendarUtils.getYearNameTitle(days.f));
                tvStartRangeTitle.setTextColor(new Color(getSelectionBarMonthTextColor()));

                Text tvEndRangeTitle = (Text) findComponentById(ResourceTable.Id_tv_range_end_date);
                tvEndRangeTitle.setText(CalendarUtils.getYearNameTitle(days.s));
                tvEndRangeTitle.setTextColor(new Color(getSelectionBarMonthTextColor()));

                Text catvStart = (Text) findComponentById(ResourceTable.Id_catv_start);
                catvStart.setText(String.valueOf(days.f.getDayNumber()));
                catvStart.setTextColor(new Color(getSelectedDayTextColor()));
                ((CircleAnimationText) catvStart).showAsStartCircle(this, true);

                Text catvEnd = (Text) findComponentById(ResourceTable.Id_catv_end);
                catvEnd.setText(String.valueOf(days.s.getDayNumber()));
                catvEnd.setTextColor(new Color(getSelectedDayTextColor()));
                ((CircleAnimationText) catvEnd).showAsEndCircle(this, true);

                Text catvMiddle = (Text) findComponentById(ResourceTable.Id_catv_middle);
                ((CircleAnimationText) catvMiddle).showAsRange(this);
            } else {
                dlRangeSelection.setVisibility(HIDE);
            }
        }
    }

    @Override
    public int getCalendarBackgroundColor() {
        return settingsManager.getCalendarBackgroundColor();
    }

    @Override
    public int getMonthTextColor() {
        return settingsManager.getMonthTextColor();
    }

    @Override
    public int getOtherDayTextColor() {
        return settingsManager.getOtherDayTextColor();
    }

    @Override
    public int getDayTextColor() {
        return settingsManager.getDayTextColor();
    }

    @Override
    public int getWeekendDayTextColor() {
        return settingsManager.getWeekendDayTextColor();
    }

    @Override
    public int getWeekDayTitleTextColor() {
        return settingsManager.getWeekDayTitleTextColor();
    }

    @Override
    public int getSelectedDayTextColor() {
        return settingsManager.getSelectedDayTextColor();
    }

    @Override
    public int getSelectedDayBackgroundColor() {
        return settingsManager.getSelectedDayBackgroundColor();
    }

    @Override
    public int getSelectedDayBackgroundStartColor() {
        return settingsManager.getSelectedDayBackgroundStartColor();
    }

    @Override
    public int getSelectedDayBackgroundEndColor() {
        return settingsManager.getSelectedDayBackgroundEndColor();
    }

    @Override
    public int getCurrentDayTextColor() {
        return settingsManager.getCurrentDayTextColor();
    }

    @Override
    public int getCurrentDayIconRes() {
        return settingsManager.getCurrentDayIconRes();
    }

    @Override
    public int getCurrentDaySelectedIconRes() {
        return settingsManager.getCurrentDaySelectedIconRes();
    }

    @Override
    public int getCalendarOrientation() {
        return settingsManager.getCalendarOrientation();
    }

    @Override
    public int getConnectedDayIconRes() {
        return settingsManager.getConnectedDayIconRes();
    }

    @Override
    public int getConnectedDaySelectedIconRes() {
        return settingsManager.getConnectedDaySelectedIconRes();
    }

    @Override
    public int getConnectedDayIconPosition() {
        return settingsManager.getConnectedDayIconPosition();
    }

    @Override
    public int getDisabledDayTextColor() {
        return settingsManager.getDisabledDayTextColor();
    }

    @Override
    public int getSelectionBarMonthTextColor() {
        return settingsManager.getSelectionBarMonthTextColor();
    }

    @Override
    public int getPreviousMonthIconRes() {
        return settingsManager.getPreviousMonthIconRes();
    }

    @Override
    public int getNextMonthIconRes() {
        return settingsManager.getNextMonthIconRes();
    }

    @Override
    public boolean isShowDaysOfWeek() {
        return settingsManager.isShowDaysOfWeek();
    }

    @Override
    public boolean isShowDaysOfWeekTitle() {
        return settingsManager.isShowDaysOfWeekTitle();
    }

    @Override
    public void setCalendarBackgroundColor(int calendarBackgroundColor) {
        settingsManager.setCalendarBackgroundColor(calendarBackgroundColor);
        setBackgroundColor(calendarBackgroundColor);
    }

    private void setBackgroundColor(int color) {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(color));
        setBackground(element);
    }

    public void update() {
        if (monthProvider != null) {
            monthProvider.notifyDataChanged();
            rvMonths.scrollTo(lastVisibleMonthPosition);
            multipleSelectionBarProvider.notifyDataChanged();
        }
    }

    @Override
    public void setMonthTextColor(int monthTextColor) {
        settingsManager.setMonthTextColor(monthTextColor);
        update();
    }

    @Override
    public void setOtherDayTextColor(int otherDayTextColor) {
        settingsManager.setOtherDayTextColor(otherDayTextColor);
        update();
    }

    @Override
    public void setDayTextColor(int dayTextColor) {
        settingsManager.setDayTextColor(dayTextColor);
        update();
    }

    @Override
    public void setWeekendDayTextColor(int weekendDayTextColor) {
        settingsManager.setWeekendDayTextColor(weekendDayTextColor);
        update();
    }

    @Override
    public void setWeekDayTitleTextColor(int weekDayTitleTextColor) {
        settingsManager.setWeekDayTitleTextColor(weekDayTitleTextColor);
        for (int i = 0; i < dlDaysOfWeekTitles.getChildCount(); i++) {
            ((Text) dlDaysOfWeekTitles.getComponentAt(i)).setTextColor(new Color(weekDayTitleTextColor));
        }
        update();
    }

    @Override
    public void setSelectedDayTextColor(int selectedDayTextColor) {
        settingsManager.setSelectedDayTextColor(selectedDayTextColor);
        update();
    }

    @Override
    public void setSelectedDayBackgroundColor(int selectedDayBackgroundColor) {
        settingsManager.setSelectedDayBackgroundColor(selectedDayBackgroundColor);
        update();
    }

    @Override
    public void setSelectedDayBackgroundStartColor(int selectedDayBackgroundStartColor) {
        settingsManager.setSelectedDayBackgroundStartColor(selectedDayBackgroundStartColor);
        update();
    }

    @Override
    public void setSelectedDayBackgroundEndColor(int selectedDayBackgroundEndColor) {
        settingsManager.setSelectedDayBackgroundEndColor(selectedDayBackgroundEndColor);
        update();
    }

    @Override
    public void setCurrentDayTextColor(int currentDayTextColor) {
        settingsManager.setCurrentDayTextColor(currentDayTextColor);
        update();
    }

    @Override
    public void setCurrentDayIconRes(int currentDayIconRes) {
        settingsManager.setCurrentDayIconRes(currentDayIconRes);
        update();
    }

    @Override
    public void setCurrentDaySelectedIconRes(int currentDaySelectedIconRes) {
        settingsManager.setCurrentDaySelectedIconRes(currentDaySelectedIconRes);
        update();
    }

    @Override
    public void setCalendarOrientation(int calendarOrientation) {
        clearSelections();
        settingsManager.setCalendarOrientation(calendarOrientation);
        setDaysOfWeekTitles();
        recreateInitialMonth();
        if (getCalendarOrientation() == Component.HORIZONTAL) {
            if (flNavigationButtons != null) {
                flNavigationButtons.setVisibility(VISIBLE);
            } else {
                initNavigationButtons();
            }
        } else {
            if (flNavigationButtons != null) {
                flNavigationButtons.setVisibility(HIDE);
            }
        }

        setSelectionBarVisibility();
        update();
    }

    public void clearSelections() {
        selectionManager.clearSelections();
        if (selectionManager instanceof MultipleSelectionManager) {
            ((MultipleSelectionManager) selectionManager).clearCriteriaList();
        }
        multipleSelectionBarProvider.setData(new ArrayList<SelectionBarItem>());
        setSelectionBarVisibility();
        update();
    }


    private void setSelectionBarVisibility() {
        flBottomSelectionBar.setVisibility(getCalendarOrientation() == Component.HORIZONTAL ? VISIBLE : HIDE);
        lcMultipleSelectedList.setVisibility(getCalendarOrientation() == Component.HORIZONTAL && getSelectionType() == SelectionType.MULTIPLE ? VISIBLE : HIDE);
        dlRangeSelection.setVisibility(needToShowSelectedDaysRange() ? VISIBLE : HIDE);
    }

    private boolean needToShowSelectedDaysRange() {
        if (getCalendarOrientation() == Component.HORIZONTAL && getSelectionType() == SelectionType.RANGE) {
            if (selectionManager instanceof RangeSelectionManager) {
                if (((RangeSelectionManager) selectionManager).getDays() != null) {
                    return true;
                }
            }
        }
        return false;
    }

    private void setDaysOfWeekTitles() {
        settingsManager.setShowDaysOfWeekTitle(settingsManager.getCalendarOrientation() != Component.HORIZONTAL);
        settingsManager.setShowDaysOfWeek(settingsManager.getCalendarOrientation() == Component.HORIZONTAL);

        if (dlDaysOfWeekTitles == null) {
            createDaysOfWeekTitle();
        }
        if (settingsManager.isShowDaysOfWeekTitle()) {
            showDaysOfWeekTitle();
        } else {
            hideDaysOfWeekTitle();
        }
    }

    private void recreateInitialMonth() {
        lastVisibleMonthPosition = SettingsManager.DEFAULT_MONTH_COUNT / 2;
        setListOrientation();
    }

    @Override
    public void setConnectedDayIconRes(int connectedDayIconRes) {
        settingsManager.setConnectedDayIconRes(connectedDayIconRes);
        update();
    }

    @Override
    public void setConnectedDaySelectedIconRes(int connectedDaySelectedIconRes) {
        settingsManager.setConnectedDaySelectedIconRes(connectedDaySelectedIconRes);
        update();
    }

    @Override
    public void setConnectedDayIconPosition(int connectedDayIconPosition) {
        settingsManager.setConnectedDayIconPosition(connectedDayIconPosition);
        update();
    }

    @Override
    public void setDisabledDayTextColor(int disabledDayTextColor) {
        settingsManager.setDisabledDayTextColor(disabledDayTextColor);
        update();
    }

    @Override
    public void setSelectionBarMonthTextColor(int selectionBarMonthTextColor) {
        settingsManager.setSelectionBarMonthTextColor(selectionBarMonthTextColor);
        update();
    }

    @Override
    public void setPreviousMonthIconRes(int previousMonthIconRes) {
        settingsManager.setPreviousMonthIconRes(previousMonthIconRes);
        setPreviousNavigationButton();
    }

    @Override
    public void setNextMonthIconRes(int nextMonthIconRes) {
        settingsManager.setNextMonthIconRes(nextMonthIconRes);
        setNextNavigationButton();
    }

    @Override
    public void setShowDaysOfWeek(boolean showDaysOfWeek) {
        settingsManager.setShowDaysOfWeek(showDaysOfWeek);
        recreateInitialMonth();
    }

    @Override
    public void setShowDaysOfWeekTitle(boolean showDaysOfWeekTitle) {
        settingsManager.setShowDaysOfWeekTitle(showDaysOfWeekTitle);
        if (showDaysOfWeekTitle) {
            showDaysOfWeekTitle();
        } else {
            hideDaysOfWeekTitle();
        }
    }

    @Override
    public int getFirstDayOfWeek() {
        return settingsManager.getFirstDayOfWeek();
    }

    @Override
    public void setFirstDayOfWeek(int firstDayOfWeek) {
        if (firstDayOfWeek > 0 && firstDayOfWeek < 8) {
            settingsManager.setFirstDayOfWeek(firstDayOfWeek);
            createDaysOfWeekTitle();
            recreateInitialMonth();
        } else {
            throw new IllegalArgumentException("First day of week must be 1 - 7");
        }
    }

    @Override
    public void onMultipleSelectionListItemClick(Day day) {
        if (getSelectionManager() instanceof MultipleSelectionManager) {
            ((MultipleSelectionManager) getSelectionManager()).removeDay(day);
            monthProvider.notifyDataChanged();
        }
    }

    @Override
    public Set<Long> getDisabledDays() {
        return settingsManager.getDisabledDays();
    }

    @Override
    public ConnectedDaysManager getConnectedDaysManager() {
        return settingsManager.getConnectedDaysManager();
    }

    @Override
    public Set<Long> getWeekendDays() {
        return settingsManager.getWeekendDays();
    }

    @Override
    public DisabledDaysCriteria getDisabledDaysCriteria() {
        return settingsManager.getDisabledDaysCriteria();
    }

    public void setDisabledDays(Set<Long> disabledDays) {
        settingsManager.setDisabledDays(disabledDays);
        monthProvider.setDisabledDays(disabledDays);
    }

    public void setWeekendDays(Set<Long> weekendDays) {
        settingsManager.setWeekendDays(weekendDays);
        monthProvider.setWeekendDays(weekendDays);
    }

    @Override
    public void setDisabledDaysCriteria(DisabledDaysCriteria criteria) {
        settingsManager.setDisabledDaysCriteria(criteria);
        monthProvider.setDisabledDaysCriteria(criteria);
    }

    @Override
    public void addConnectedDays(ConnectedDays connectedDays) {
        settingsManager.getConnectedDaysManager().addConnectedDays(connectedDays);
        recreateInitialMonth();
    }

    @Override
    public void onMonthChanged(Month month) {

    }
}
