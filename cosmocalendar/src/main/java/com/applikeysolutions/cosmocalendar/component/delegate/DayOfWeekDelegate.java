package com.applikeysolutions.cosmocalendar.component.delegate;

import com.applikeysolutions.cosmocalendar.ResourceTable;
import com.applikeysolutions.cosmocalendar.component.CalendarComponent;
import com.applikeysolutions.cosmocalendar.model.Day;
import com.applikeysolutions.cosmocalendar.utils.Constants;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DayOfWeekDelegate extends BaseDelegate {

    private final SimpleDateFormat mDayOfWeekFormatter;

    public DayOfWeekDelegate(CalendarComponent calendarView) {
        this.calendarView = calendarView;
        mDayOfWeekFormatter = new SimpleDateFormat(Constants.DAY_NAME_FORMAT, Locale.getDefault());
    }

    public Component onParse(LayoutScatter layoutScatter, ComponentContainer root, int itemType) {
        Component component = layoutScatter.parse(ResourceTable.Layout_item_day_of_week, root, false);
        return component;
    }

    public void onBindData(Day day, Component Component, int position) {
        Text text = (Text) Component.findComponentById(ResourceTable.Id_tv_day_name);
        text.setText(mDayOfWeekFormatter.format(day.getCalendar().getTime()));
    }
}
