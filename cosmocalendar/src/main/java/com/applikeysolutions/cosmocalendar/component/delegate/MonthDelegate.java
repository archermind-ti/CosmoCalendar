package com.applikeysolutions.cosmocalendar.component.delegate;

import com.applikeysolutions.cosmocalendar.ResourceTable;
import com.applikeysolutions.cosmocalendar.model.Month;
import com.applikeysolutions.cosmocalendar.provider.DaysProvider;
import com.applikeysolutions.cosmocalendar.settings.SettingsManager;
import com.applikeysolutions.cosmocalendar.utils.Constants;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

public class MonthDelegate {

    private SettingsManager appearanceModel;

    public MonthDelegate(SettingsManager appearanceModel) {
        this.appearanceModel = appearanceModel;
    }

    public Component onParse(LayoutScatter layoutScatter, ComponentContainer parent, int itemType) {
        Component component = layoutScatter.parse(ResourceTable.Layout_item_month, parent, false);
        return component;
    }

    public void onBindData(DaysProvider adapter, Month month, Component component, int position) {
        ListContainer listContainer = (ListContainer) component.findComponentById(ResourceTable.Id_month_view);
        listContainer.setLongClickable(false);
        DirectionalLayout dlMonthHeader = (DirectionalLayout) component.findComponentById(ResourceTable.Id_dl_month_header);
        Text monthName = (Text) component.findComponentById(ResourceTable.Id_tv_month_name);

        monthName.setText(month.getMonthName());
        monthName.setTextColor(new Color(appearanceModel.getMonthTextColor()));

        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(Constants.DAYS_IN_WEEK);
        if (appearanceModel.getCalendarOrientation() == Component.HORIZONTAL) {
            listContainer.setOrientation(Component.VERTICAL);
        } else {
            listContainer.setOrientation(Component.HORIZONTAL);
        }
        listContainer.setLayoutManager(tableLayoutManager);
        listContainer.setItemProvider(adapter);
        adapter.setMonth(month);
    }
}
