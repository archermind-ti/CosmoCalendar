package com.applikeysolutions.cosmocalendar.component;

public enum ItemType {
    MONTH(0), MONTH_DAY(1), OTHER_MONTH_DAY(2), DAY_OF_WEEK(3);

    private int value;

    ItemType(int i) {
        value = i;
    }

    public int getValue() {
        return value;
    }

    public static ItemType getType(int value) {
        if (value == MONTH.getValue()) {
            return MONTH;
        } else if (value == MONTH_DAY.getValue()) {
            return MONTH_DAY;
        } else if (value == OTHER_MONTH_DAY.getValue()) {
            return OTHER_MONTH_DAY;
        } else if (value == DAY_OF_WEEK.getValue()) {
            return DAY_OF_WEEK;
        } else {
            return MONTH;
        }
    }


}