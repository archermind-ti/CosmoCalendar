package com.applikeysolutions.cosmocalendar.component;

import com.applikeysolutions.cosmocalendar.model.Day;
import com.applikeysolutions.cosmocalendar.selection.SelectionState;
import com.applikeysolutions.cosmocalendar.utils.CalendarUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;

public class CircleAnimationText extends Text implements
        Component.EstimateSizeListener, Component.DrawTask {

    private SelectionState selectionState;
    private CalendarComponent calendarView;

    private int animationProgress;
    private boolean clearView;
    private boolean stateChanged;

    //Circle
    private Paint circlePaint;
    private Paint circleUnderPaint;
    private Day day;
    private int circleColor;

    //Variable to fix bugs when cannot start animation during scroll/fast scroll
    //seems like animation can't be done on views that are not visible on screen
    private boolean animationStarted;
    private long animationStartTime;

    //Start/End range half rectangle
    private Paint rectanglePaint;
    private Rect rectangle;

    //Rectangle
    private Paint backgroundRectanglePaint;
    private Rect backgroundRectangle;

    public static final int DEFAULT_PADDING = 10;
    public static final int MAX_PROGRESS = 100;
    public static final long SELECTION_ANIMATION_DURATION = 300;

    public CircleAnimationText(Context context) {
        this(context, null);
    }

    public CircleAnimationText(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public CircleAnimationText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        setEstimateSizeListener(this);
        addDrawTask(this, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (clearView) {
            clearVariables();
        }

        if (selectionState != null) {
            switch (selectionState) {
                case START_RANGE_DAY:
                case END_RANGE_DAY:
                    drawRectangle(canvas);
                    drawCircleUnder(canvas);
                    drawCircle(canvas);
                    break;

                case START_RANGE_DAY_WITHOUT_END:
                    drawCircle(canvas);
                    break;

                case SINGLE_DAY:
                    //Animation not started yet
                    //progress not MAX_PROGRESS
                    boolean condition1 = !animationStarted
                            && animationProgress != MAX_PROGRESS;

                    //Animation started
                    //but was terminated (more than SELECTION_ANIMATION_DURATION have passed from animationStartTime)
                    //progress not MAX_PROGRESS
                    long currentTime = System.currentTimeMillis();
                    boolean condition2 = animationStarted
                            && (currentTime > animationStartTime + SELECTION_ANIMATION_DURATION)
                            && animationProgress != MAX_PROGRESS;

                    if (condition1 || condition2) {
                        animateView();
                    } else {
                        drawCircle(canvas);
                    }
                    break;

                case RANGE_DAY:
                    //区域选择绘制底部背景
                    drawBackgroundRectangle(canvas);
                    break;
            }
        }
    }

    //Square view
    @Override
    public boolean onEstimateSize(int i, int i1) {
        int width = Component.EstimateSpec.getSize(i);
        int widthMode = Component.EstimateSpec.getMode(i);
        int height = Component.EstimateSpec.getSize(i1);
        int heightMode = Component.EstimateSpec.getMode(i1);
        width = CalendarUtils.getCircleWidth(getContext());
        setEstimatedSize(Component.EstimateSpec.getSizeWithMode(width, Component.EstimateSpec.PRECISE), Component.EstimateSpec.getSizeWithMode(width, Component.EstimateSpec.PRECISE));
        return true;
    }

    private void drawCircle(Canvas canvas) {
        if (animationProgress == 100) {
            if (day != null) {
                day.setSelectionCircleDrawed(true);
            }
        }
        if (circlePaint == null || stateChanged) {
            createCirclePaint();
        }

        final int diameter = getWidth() - DEFAULT_PADDING * 2;
        final int diameterProgress = animationProgress * diameter / MAX_PROGRESS;

        setBackgroundColor(Color.TRANSPARENT);
        canvas.drawCircle(getWidth() / 2, getWidth() / 2, diameterProgress / 2, circlePaint);
    }

    private void drawCircleUnder(Canvas canvas) {
        if (circleUnderPaint == null || stateChanged) {
            createCircleUnderPaint();
        }
        final int diameter = getWidth() - DEFAULT_PADDING * 2;
        canvas.drawCircle(getWidth() / 2, getWidth() / 2, diameter / 2, circleUnderPaint);
    }

    private void createCirclePaint() {
        circlePaint = new Paint();
        circlePaint.setColor(new Color(circleColor));
        circlePaint.setAntiAlias(true);
    }

    private void createCircleUnderPaint() {
        circleUnderPaint = new Paint();
        circleUnderPaint.setColor(new Color(calendarView.getSelectedDayBackgroundColor()));
        circleUnderPaint.setAntiAlias(true);
    }

    private void drawRectangle(Canvas canvas) {
        if (rectanglePaint == null) {
            createRectanglePaint();
        }
        if (rectangle == null) {
            rectangle = getRectangleForState();
        }
        canvas.drawRect(rectangle, rectanglePaint);
    }

    private void createRectanglePaint() {
        rectanglePaint = new Paint();
        rectanglePaint.setColor(new Color(calendarView.getSelectedDayBackgroundColor()));
        rectanglePaint.setAntiAlias(true);
    }

    private void drawBackgroundRectangle(Canvas canvas) {
        if (backgroundRectanglePaint == null) {
            createBackgroundRectanglePaint();
        }
        if (backgroundRectangle == null) {
            backgroundRectangle = getRectangleForState();
        }
        canvas.drawRect(backgroundRectangle, backgroundRectanglePaint);
    }

    private void createBackgroundRectanglePaint() {
        backgroundRectanglePaint = new Paint();
        backgroundRectanglePaint.setColor(new Color(calendarView.getSelectedDayBackgroundColor()));
        backgroundRectanglePaint.setAntiAlias(true);
    }

    public SelectionState getSelectionState() {
        return selectionState;
    }

    public void setSelectionStateAndAnimate(SelectionState state, CalendarComponent calendarView, Day day) {
        isStateChanged(state);
        selectionState = state;
        this.calendarView = calendarView;
        day.setSelectionState(state);
        this.day = day;

        if (selectionState != null && calendarView != null) {
            switch (selectionState) {
                case START_RANGE_DAY:
                    circleColor = calendarView.getSelectedDayBackgroundStartColor();
                    break;

                case END_RANGE_DAY:
                    circleColor = calendarView.getSelectedDayBackgroundEndColor();
                    break;

                case START_RANGE_DAY_WITHOUT_END:
                    setBackgroundColor(Color.TRANSPARENT);
                    circleColor = calendarView.getSelectedDayBackgroundStartColor();
                    break;

                case SINGLE_DAY:
                    circleColor = calendarView.getSelectedDayBackgroundColor();
                    setBackgroundColor(Color.TRANSPARENT);
                    break;
            }
        }
        animateView();
    }

    private Rect getRectangleForState() {
        switch (selectionState) {
            case START_RANGE_DAY:
                return new Rect(getWidth() / 2, DEFAULT_PADDING, getWidth(), getHeight() - DEFAULT_PADDING);

            case END_RANGE_DAY:
                return new Rect(0, DEFAULT_PADDING, getWidth() / 2, getHeight() - DEFAULT_PADDING);

            case RANGE_DAY:
                return new Rect(0, DEFAULT_PADDING, getWidth(), getHeight() - DEFAULT_PADDING);

            default:
                return null;
        }
    }

    private void animateView() {
        AnimatorValue animation = new AnimatorValue();
        animation.setLoopedCount(0);
        animation.setDuration(SELECTION_ANIMATION_DURATION);
        animation.setValueUpdateListener((animatorValue, v) -> {
            int progress = (int) (v * 100);
            CircleAnimationText.this.setAnimationProgress(100);
            invalidate();
        });
        animation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                animationStarted = true;
                animationStartTime = System.currentTimeMillis();
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                animationStarted = false;
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animation.start();
    }

    private void isStateChanged(SelectionState newState) {
        stateChanged = (selectionState == null || selectionState != newState);
    }

    public void setAnimationProgress(int animationProgress) {
        this.animationProgress = animationProgress;
    }

    public void clearView() {
        if (selectionState != null) {
            clearView = true;
            invalidate();
        }
    }

    private void clearVariables() {
        selectionState = null;
        calendarView = null;
        circlePaint = null;
        rectanglePaint = null;
        rectangle = null;

        stateChanged = false;

        circleColor = 0;

        animationProgress = 0;
        animationStarted = false;
        animationStartTime = 0;

        setBackgroundColor(Color.TRANSPARENT);

        clearView = false;
    }

    private void setBackgroundColor(Color color) {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
        setBackground(element);
    }

    public void showAsCircle(int circleColor) {
        this.circleColor = circleColor;
        animationProgress = 100;
        setWidth(CalendarUtils.getCircleWidth(getContext()));
        setHeight(CalendarUtils.getCircleWidth(getContext()));
        invalidate();
    }

    public void showAsSingleCircle(CalendarComponent calendarView) {
        clearVariables();
        selectionState = SelectionState.SINGLE_DAY;
        showAsCircle(calendarView.getSelectedDayBackgroundColor());
    }

    public void showAsStartCircle(CalendarComponent calendarView, boolean animate) {
        if (animate) {
            clearVariables();
        }
        this.calendarView = calendarView;
        selectionState = SelectionState.START_RANGE_DAY;
        showAsCircle(calendarView.getSelectedDayBackgroundStartColor());
    }

    public void showAsStartCircleWithouEnd(CalendarComponent calendarView, boolean animate) {
        if (animate) {
            clearVariables();
        }
        this.calendarView = calendarView;
        selectionState = SelectionState.START_RANGE_DAY_WITHOUT_END;
        showAsCircle(calendarView.getSelectedDayBackgroundStartColor());
    }

    public void showAsEndCircle(CalendarComponent calendarView, boolean animate) {
        if (animate) {
            clearVariables();
        }
        this.calendarView = calendarView;
        selectionState = SelectionState.END_RANGE_DAY;
        showAsCircle(calendarView.getSelectedDayBackgroundEndColor());
    }

    public void showAsRange(CalendarComponent calendarView) {
        clearVariables();
        this.calendarView = calendarView;
        selectionState = SelectionState.RANGE_DAY;
        setWidth(CalendarUtils.getCircleWidth(getContext()) / 2);
        setHeight(CalendarUtils.getCircleWidth(getContext()));
        invalidate();
    }
}
