package com.applikeysolutions.cosmocalendar.selection;

import com.applikeysolutions.cosmocalendar.model.Day;
import org.jetbrains.annotations.NotNull;

public class NoneSelectionManager extends BaseSelectionManager {

    @Override
    public void toggleDay(@NotNull Day day) {

    }

    @Override
    public boolean isDaySelected(@NotNull Day day) {
        return false;
    }

    @Override
    public void clearSelections() {

    }
}
