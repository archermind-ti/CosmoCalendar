package com.applikeysolutions.cosmocalendar.selection.selectionbar;

import com.applikeysolutions.cosmocalendar.ResourceTable;
import com.applikeysolutions.cosmocalendar.component.CalendarComponent;
import com.applikeysolutions.cosmocalendar.component.CircleAnimationText;
import com.applikeysolutions.cosmocalendar.model.Day;
import com.applikeysolutions.cosmocalendar.provider.BaseListProvider;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class MultipleSelectionBarProvider extends BaseListProvider<SelectionBarItem> {

    private final CalendarComponent calendarView;
    private ListItemClickListener listItemClickListener;
    private final static int VIEW_TYPE_TITLE = 0;
    private final static int VIEW_TYPE_CONTENT = 1;

    public MultipleSelectionBarProvider(Context context, CalendarComponent calendarView, ListItemClickListener listItemClickListener) {
        super(context);
        this.calendarView = calendarView;
        this.listItemClickListener = listItemClickListener;
    }

    @Override
    public Component onParse(int position, LayoutScatter layoutScatter, ComponentContainer root) {
        int type = getItemComponentType(position);
        if (type == VIEW_TYPE_TITLE) {
            return layoutScatter.parse(ResourceTable.Layout_item_multiple_selection_bar_title, root, false);
        } else {
            Component component = layoutScatter.parse(ResourceTable.Layout_item_multiple_selection_bar_content, root, false);
            return component;
        }
    }

    @Override
    public Component onParse(LayoutScatter layoutScatter, ComponentContainer root) {
        return null;
    }

    @Override
    public void onBindData(Component component, int position) {
        int type = getItemComponentType(position);
        if (type == VIEW_TYPE_TITLE) {
            Text title = (Text) component.findComponentById(ResourceTable.Id_tv_title);
            SelectionBarTitleItem selectionBarTitleItem = (SelectionBarTitleItem) mDataList.get(position);
            title.setText(selectionBarTitleItem.getTitle());
            title.setTextColor(new Color(calendarView.getSelectionBarMonthTextColor()));
        } else {
            Text content = (Text) component.findComponentById(ResourceTable.Id_catv_day);
            final SelectionBarContentItem selectionBarContentItem = (SelectionBarContentItem) mDataList.get(position);
            content.setText(String.valueOf(selectionBarContentItem.getDay().getDayNumber()));
            content.setTextColor(new Color(calendarView.getSelectedDayTextColor()));
            ((CircleAnimationText) content).showAsSingleCircle(calendarView);
            content.setClickedListener(component1 -> {
                if (listItemClickListener != null) {
                    listItemClickListener.onMultipleSelectionListItemClick(selectionBarContentItem.getDay());
                }
            });
        }
    }

    @Override
    public void onBindData(Component component, SelectionBarItem itemData) {

    }

    @Override
    public int getItemComponentType(int position) {
        if (mDataList.get(position) instanceof SelectionBarTitleItem) {
            return VIEW_TYPE_TITLE;
        } else {
            return VIEW_TYPE_CONTENT;
        }
    }

    public void setListItemClickListener(ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    public interface ListItemClickListener {
        void onMultipleSelectionListItemClick(Day day);
    }
}
