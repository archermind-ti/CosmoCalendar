package com.applikeysolutions.cosmocalendar.selection;


import com.applikeysolutions.cosmocalendar.model.Day;
import org.jetbrains.annotations.NotNull;

public abstract class BaseSelectionManager {

    protected OnDaySelectedListener onDaySelectedListener;

    public abstract void toggleDay(@NotNull Day day);

    public abstract boolean isDaySelected(@NotNull Day day);

    public abstract void clearSelections();

    public BaseSelectionManager() {
    }
}
