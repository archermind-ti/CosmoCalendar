package com.applikeysolutions.cosmocalendar.utils;

public enum SelectionType {
    SINGLE(0), MULTIPLE(1), RANGE(2), NONE(3);

    private int value;

    SelectionType(int i) {
        value = i;
    }

    public int getValue() {
        return value;
    }

    public static SelectionType getType(int value) {
        if (value == SINGLE.getValue()) {
            return SINGLE;
        } else if (value == MULTIPLE.getValue()) {
            return MULTIPLE;
        } else if (value == RANGE.getValue()) {
            return RANGE;
        } else if (value == NONE.getValue()) {
            return NONE;
        } else {
            return SINGLE;
        }
    }
}
