package com.applikeysolutions.cosmocalendar.provider;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseListProvider<D> extends BaseItemProvider {

    public List<D> mDataList = new ArrayList<>();
    protected Context mContext;

    public BaseListProvider(Context context) {
        super();
        mContext = context;
    }

    public void setData(List<D> data) {
        if (data == null) {
            return;
        }
        if (mDataList != null) {
            if (mDataList.size() > 0) {
                mDataList.clear();
                mDataList.addAll(data);
                notifyDataChanged();
                return;
            } else {
                mDataList.addAll(data);
            }
        } else {
            mDataList = data;
        }
        notifyDataSetItemInserted(0);
    }

    public List<D> getData() {
        return mDataList;
    }

    public boolean append(List<D> data) {
        return append(data, (mDataList != null && mDataList.size() > 0) ? mDataList.size() : 0);
    }

    public boolean append(List<D> data, int index) {
        boolean appendOK = false;
        if (mDataList != null && data != null) {
            appendOK = mDataList.addAll(index, data);
            if (appendOK) {
                notifyDataSetItemRangeInserted(index, data.size());
            }
        }
        return appendOK;
    }

    public void clear() {
        if (mDataList != null) {
            mDataList.clear();
            notifyDataChanged();
        }
    }

    @Override
    public int getCount() {
        if (mDataList != null) {
            return mDataList.size();
        }
        return 0;
    }

    @Override
    public D getItem(int i) {
        if (mDataList.size() > 0 && i < mDataList.size()) {
            return mDataList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public Component onParse(int position, LayoutScatter layoutScatter, ComponentContainer root) {
        return onParse(layoutScatter, root);
    }

    public abstract Component onParse(LayoutScatter layoutScatter, ComponentContainer root);

    public abstract void onBindData(Component component, D itemData);

    public void onBindData(Component component, int position) {
        onBindData(component, mDataList.get(position));
    }

    @Override
    public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        int itemType = getItemComponentType(i);
        int convertItemType = (convertComponent != null && convertComponent.getTag() != null) ?
                (int) (convertComponent.getTag()) : -1;
        if (convertComponent != null && convertItemType == itemType) {
            cpt = convertComponent;
        } else {
            cpt = onParse(i, LayoutScatter.getInstance(mContext), componentContainer);
        }
        cpt.setId(i);
        cpt.setTag(getItemComponentType(i));
        onBindData(cpt, i);
        return cpt;
    }

    @Override
    public int getItemComponentType(int position) {
        return 0;
    }
}
