package com.applikeysolutions.cosmocalendar.provider;

import com.applikeysolutions.cosmocalendar.component.CalendarComponent;
import com.applikeysolutions.cosmocalendar.component.ItemType;
import com.applikeysolutions.cosmocalendar.component.delegate.DayDelegate;
import com.applikeysolutions.cosmocalendar.component.delegate.DayOfWeekDelegate;
import com.applikeysolutions.cosmocalendar.component.delegate.MonthDelegate;
import com.applikeysolutions.cosmocalendar.component.delegate.OtherDayDelegate;
import com.applikeysolutions.cosmocalendar.model.Day;
import com.applikeysolutions.cosmocalendar.model.Month;
import com.applikeysolutions.cosmocalendar.selection.BaseSelectionManager;
import com.applikeysolutions.cosmocalendar.settings.lists.DisabledDaysCriteria;
import com.applikeysolutions.cosmocalendar.utils.CalendarUtils;
import com.applikeysolutions.cosmocalendar.utils.DayFlag;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

public class MonthProvider extends BaseListProvider<Month> {

    private MonthDelegate mMonthDelegate;
    private CalendarComponent mCalendarComponent;
    private BaseSelectionManager mSelectionManager;
    private int orientation;
    private DaysProvider daysAdapter;

    public MonthProvider(Context context,
                         int orientation,
                         List<Month> months,
                         MonthDelegate monthDelegate,
                         CalendarComponent calendarComponent,
                         BaseSelectionManager selectionManager
    ) {
        super(context);
        this.orientation = orientation;
        mDataList = months;
        mMonthDelegate = monthDelegate;
        mCalendarComponent = calendarComponent;
        mSelectionManager = selectionManager;
    }

    public CalendarComponent getCalendarComponent() {
        return mCalendarComponent;
    }

    public void setSelectionManager(BaseSelectionManager selectionManager) {
        mSelectionManager = selectionManager;
    }

    public BaseSelectionManager getSelectionManager() {
        return mSelectionManager;
    }

    @Override
    public Component onParse(int position, LayoutScatter layoutScatter, ComponentContainer root) {
        int w = AttrHelper.fp2px(mContext.getResourceManager().getDeviceCapability().width, mContext);
        Component component = mMonthDelegate.onParse(layoutScatter, root, getItemComponentType(position));
        component.setWidth(w);
        return component;
    }

    @Override
    public Component onParse(LayoutScatter layoutScatter, ComponentContainer root) {
        return null;
    }

    @Override
    public void onBindData(Component component, int position) {
        daysAdapter = new DaysProvider.DaysAdapterBuilder()
                .setContext(mContext)
                .setDayOfWeekDelegate(new DayOfWeekDelegate(mCalendarComponent))
                .setOtherDayDelegate(new OtherDayDelegate(mCalendarComponent))
                .setDayDelegate(new DayDelegate(mCalendarComponent, this))
                .setCalendarView(mCalendarComponent)
                .createDaysAdapter();

        mMonthDelegate.onBindData(daysAdapter, mDataList.get(position), component, position);
    }

    @Override
    public void onBindData(Component component, Month itemData) {
    }

    @Override
    public long getItemId(int position) {
        return mDataList.get(position).getFirstDay().getCalendar().getTimeInMillis();
    }

    @Override
    public int getItemComponentType(int position) {
        return ItemType.MONTH.getValue();
    }

    public static class MonthAdapterBuilder {

        private List<Month> months;
        private MonthDelegate monthDelegate;
        private CalendarComponent calendarView;
        private BaseSelectionManager selectionManager;
        private int orientation;

        public MonthAdapterBuilder setOrientation(int orientation) {
            this.orientation = orientation;
            return this;
        }

        public MonthAdapterBuilder setMonths(List<Month> months) {
            this.months = months;
            return this;
        }

        public MonthAdapterBuilder setMonthDelegate(MonthDelegate monthHolderDelegate) {
            this.monthDelegate = monthHolderDelegate;
            return this;
        }

        public MonthAdapterBuilder setCalendarView(CalendarComponent calendarView) {
            this.calendarView = calendarView;
            return this;
        }

        public MonthAdapterBuilder setSelectionManager(BaseSelectionManager selectionManager) {
            this.selectionManager = selectionManager;
            return this;
        }

        public MonthProvider createMonthProvider() {
            return new MonthProvider(
                    calendarView.getContext(),
                    orientation,
                    months,
                    monthDelegate,
                    calendarView,
                    selectionManager);
        }
    }

    public void setWeekendDays(Set<Long> weekendDays) {
        setDaysAccordingToSet(weekendDays, DayFlag.WEEKEND);
    }

    public void setDisabledDays(Set<Long> disabledDays) {
        setDaysAccordingToSet(disabledDays, DayFlag.DISABLED);
    }

    public void setConnectedCalendarDays(Set<Long> connectedCalendarDays) {
        setDaysAccordingToSet(connectedCalendarDays, DayFlag.FROM_CONNECTED_CALENDAR);
    }

    public void setDisabledDaysCriteria(DisabledDaysCriteria criteria) {
        for (Month month : mDataList) {
            for (Day day : month.getDays()) {
                if (!day.isDisabled()) {
                    day.setDisabled(CalendarUtils.isDayDisabledByCriteria(day, criteria));
                }
            }
        }
        notifyDataChanged();
    }

    private void setDaysAccordingToSet(Set<Long> days, DayFlag dayFlag) {
        if (days != null && !days.isEmpty()) {
            for (Month month : mDataList) {
                for (Day day : month.getDays()) {
                    switch (dayFlag) {
                        case WEEKEND:
                            day.setWeekend(days.contains(day.getCalendar().get(Calendar.DAY_OF_WEEK)));
                            break;

                        case DISABLED:
                            day.setDisabled(CalendarUtils.isDayInSet(day, days));
                            break;

                        case FROM_CONNECTED_CALENDAR:
                            day.setFromConnectedCalendar(CalendarUtils.isDayInSet(day, days));
                            break;
                    }
                }
            }
            notifyDataChanged();
        }
    }
}
