package com.applikeysolutions.cosmocalendar.provider;

import com.applikeysolutions.cosmocalendar.component.CalendarComponent;
import com.applikeysolutions.cosmocalendar.component.ItemType;
import com.applikeysolutions.cosmocalendar.component.delegate.DayDelegate;
import com.applikeysolutions.cosmocalendar.component.delegate.DayOfWeekDelegate;
import com.applikeysolutions.cosmocalendar.component.delegate.OtherDayDelegate;
import com.applikeysolutions.cosmocalendar.model.Day;
import com.applikeysolutions.cosmocalendar.model.Month;
import com.applikeysolutions.cosmocalendar.utils.Constants;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

public class DaysProvider extends BaseListProvider<Day> {

    private DayOfWeekDelegate dayOfWeekDelegate;
    private DayDelegate dayDelegate;
    private OtherDayDelegate otherDayDelegate;
    private CalendarComponent calendarView;

    public DaysProvider(Context context,
                        Month month,
                        DayOfWeekDelegate dayOfWeekDelegate,
                        DayDelegate dayDelegate,
                        OtherDayDelegate otherDayDelegate,
                        CalendarComponent calendarView) {
        super(context);
        if (month != null) {
            mDataList = month.getDays();
        }
        this.dayOfWeekDelegate = dayOfWeekDelegate;
        this.dayDelegate = dayDelegate;
        this.otherDayDelegate = otherDayDelegate;
        this.calendarView = calendarView;
    }

    @Override
    public Component onParse(int i, LayoutScatter layoutScatter, ComponentContainer parent) {
        int screenWidth = AttrHelper.fp2px(mContext.getResourceManager().getDeviceCapability().width, mContext);
        int width = screenWidth / Constants.DAYS_IN_WEEK;
        Component component;
        int itemType = getItemComponentType(i);
        ItemType type = ItemType.getType(itemType);
        switch (type) {
            case DAY_OF_WEEK:
                component = dayOfWeekDelegate.onParse(layoutScatter, parent, itemType);
                break;
            case MONTH_DAY:
                component = dayDelegate.onParse(layoutScatter, parent, itemType);
                break;
            case OTHER_MONTH_DAY:
                component = otherDayDelegate.onParse(layoutScatter, parent, itemType);
                break;
            default:
                throw new IllegalArgumentException("Unknown view type");
        }
        component.setWidth(width);
        component.setHeight(width);
        return component;
    }

    @Override
    public Component onParse(LayoutScatter layoutScatter, ComponentContainer root) {
        return null;
    }

    @Override
    public void onBindData(Component component, int position) {
        final Day day = mDataList.get(position);
        int type = getItemComponentType(position);
        ItemType itemType = ItemType.getType(type);
        switch (itemType) {
            case DAY_OF_WEEK:
                dayOfWeekDelegate.onBindData(day, component, position);
                break;
            case OTHER_MONTH_DAY:
                otherDayDelegate.onBindData(day, component, position);
                break;
            case MONTH_DAY:
                dayDelegate.onBindData(this, day, component, position);
                break;
        }
    }

    @Override
    public void onBindData(Component component, Day itemData) {
    }

    public void setMonth(Month month) {
        if (month != null) {
            setData(month.getDays());
        }
    }

    @Override
    public int getItemComponentType(int position) {
        if (position < Constants.DAYS_IN_WEEK && calendarView.isShowDaysOfWeek()) {
            return ItemType.DAY_OF_WEEK.getValue();
        }
        if (mDataList.get(position).isBelongToMonth()) {
            return ItemType.MONTH_DAY.getValue();
        } else {
            return ItemType.OTHER_MONTH_DAY.getValue();
        }
    }

    @Override
    public long getItemId(int position) {
        return mDataList.get(position).getCalendar().getTimeInMillis();
    }

    public static class DaysAdapterBuilder {

        private Month month;
        private DayOfWeekDelegate dayOfWeekDelegate;
        private DayDelegate dayDelegate;
        private OtherDayDelegate anotherDayDelegate;
        private CalendarComponent calendarView;
        private Context context;

        public DaysAdapterBuilder setContext(Context context) {
            this.context = context;
            return this;
        }

        public DaysAdapterBuilder setMonth(Month month) {
            this.month = month;
            return this;
        }

        public DaysAdapterBuilder setDayOfWeekDelegate(DayOfWeekDelegate dayOfWeekDelegate) {
            this.dayOfWeekDelegate = dayOfWeekDelegate;
            return this;
        }

        public DaysAdapterBuilder setDayDelegate(DayDelegate dayDelegate) {
            this.dayDelegate = dayDelegate;
            return this;
        }

        public DaysAdapterBuilder setOtherDayDelegate(OtherDayDelegate anotherDayDelegate) {
            this.anotherDayDelegate = anotherDayDelegate;
            return this;
        }

        public DaysAdapterBuilder setCalendarView(CalendarComponent calendarView) {
            this.calendarView = calendarView;
            return this;
        }

        public DaysProvider createDaysAdapter() {
            return new DaysProvider(
                    context,
                    month,
                    dayOfWeekDelegate,
                    dayDelegate,
                    anotherDayDelegate,
                    calendarView);
        }
    }
}


