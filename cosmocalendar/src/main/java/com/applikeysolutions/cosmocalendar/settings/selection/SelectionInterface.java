package com.applikeysolutions.cosmocalendar.settings.selection;


import com.applikeysolutions.cosmocalendar.utils.SelectionType;

public interface SelectionInterface {

    SelectionType getSelectionType();

    void setSelectionType(SelectionType selectionType);
}
