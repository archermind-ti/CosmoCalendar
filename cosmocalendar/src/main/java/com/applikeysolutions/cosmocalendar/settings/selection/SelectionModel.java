package com.applikeysolutions.cosmocalendar.settings.selection;


import com.applikeysolutions.cosmocalendar.utils.SelectionType;

public class SelectionModel implements SelectionInterface {

    //Selecton type SINGLE, MULTIPLE, RANGE, NONE
    private SelectionType selectionType;

    @Override
    public SelectionType getSelectionType() {
        return selectionType;
    }

    @Override
    public void setSelectionType(SelectionType selectionType) {
        this.selectionType = selectionType;
    }
}
