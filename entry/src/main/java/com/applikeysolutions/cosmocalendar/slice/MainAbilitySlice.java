package com.applikeysolutions.cosmocalendar.slice;

import com.applikeysolutions.cosmocalendar.ResourceTable;
import com.applikeysolutions.cosmocalendar.component.CalendarComponent;
import com.applikeysolutions.cosmocalendar.utils.SelectionType;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.RadioContainer;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLogLabel;


public class MainAbilitySlice extends AbilitySlice {
    private static final HiLogLabel TAG = new HiLogLabel(3, 0xD001100, "MainAbilitySlice");
    private CalendarComponent calendar;
    private RadioContainer rcOrientation, rcSelected;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    public void initView() {
        calendar = (CalendarComponent) findComponentById(ResourceTable.Id_calendar);
        rcOrientation = (RadioContainer) findComponentById(ResourceTable.Id_rc_orientation);
        rcSelected = (RadioContainer) findComponentById(ResourceTable.Id_rc_selected);
        Button btnClear = (Button) findComponentById(ResourceTable.Id_btn_clear);
        Button btnLog  = (Button) findComponentById(ResourceTable.Id_btn_log);
        rcSelected.mark(calendar.getSelectionType().getValue());
        rcSelected.setMarkChangedListener((radioContainer, i) -> {
            calendar.setSelectionType(SelectionType.getType(i));
        });
        rcOrientation.mark(calendar.getCalendarOrientation() == Component.HORIZONTAL ? 0 : 1);
        rcOrientation.setMarkChangedListener((radioContainer, i) -> {
            if (i == 0) {
                calendar.setCalendarOrientation(Component.HORIZONTAL);
            } else if (i == 1) {
                calendar.setCalendarOrientation(Component.VERTICAL);
            }
        });
        btnClear.setClickedListener(component -> clearSelectionsMenuClick());
        btnLog.setClickedListener(component -> logSelectedDaysMenuClick());
    }

    private void clearSelectionsMenuClick() {
        calendar.clearSelections();
    }

    private void logSelectedDaysMenuClick() {
        ToastDialog toastDialog = new ToastDialog(this);
        toastDialog.setText("Selected " + calendar.getSelectedDays().size());
        toastDialog.show();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
