# CosmoCalendar

## 简介

高度自定义的日历库，UI精美，支持多种模式.

## 功能
1.日期单选，多选，区域选择功能
2.日期横向水平样式设置
3.日历组件样式定制

## 安装教程

方式一

1. 下载har包[cosmocalendar-release.har](https://gitee.com/archermind-ti/CosmoCalendar/releases/V1.0.0-release)。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

方式二

```
allprojects {
    repositories {
        mavenCentral()        
    }
}

...
dependencies {
	...
	implementation 'com.gitee.archermind-ti:cosmocalendar_ohos:1.0.0'
	...
}
```

## 演示
<img src="./demo/1.png" width="20%" height="20%">
<img src="./demo/2.png" width="20%" height="20%">
<img src="./demo/3.png" width="20%" height="20%">
<img src="./demo/4.png" width="20%" height="20%">
<img src="./demo/5.png" width="20%" height="20%">
<img src="./demo/6.jpg" width="20%" height="20%">


## 使用说明
- xml中引入控件
```
 <com.applikeysolutions.cosmocalendar.component.CalendarComponent
        ohos:height="match_parent"
        ohos:width="match_parent"
        />

```

- 切换水平日历样式
```
 calendar.setCalendarOrientation(Component.HORIZONTAL);

```

- 切换垂直日历样式
```
 calendar.setCalendarOrientation(Component.VERTICAL);

```

- 切换日历选择模式
```
 calendar.setSelectionType(SelectionType.SINGLE);
 calendar.setSelectionType(SelectionType.MULTIPLE);
 calendar.setSelectionType(SelectionType.RANGE);
 calendar.setSelectionType(SelectionType.NONE);

```

- 其他日历样式修改
可参考api

## 编译说明

1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击`Run`运行即可（真机运行可能需要配置签名）

## 版本迭代
- v1.0   初始版本
[changelog](https://gitee.com/archermind-ti/CosmoCalendar/blob/master/changelog.md)

## 版权和许可信息
```
MIT License

Copyright (c) 2017 Applikey Solutions

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```